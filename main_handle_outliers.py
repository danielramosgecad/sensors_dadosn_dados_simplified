import tensorflow as tf

from controller_handle_outliers import HandleOutliersController

def main(unused_argv):
    controller = HandleOutliersController()
    controller.handle_data()

if __name__ == "__main__":
    tf.app.run()
