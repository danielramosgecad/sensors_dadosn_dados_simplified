import pandas as pd
import numpy as np
import os

COL_CONS_Z1           = 'Consumption_Zone_1_total'
COL_DALI_LIGHT_Z1     = 'DALI Lights Values_Light_Zone1'
COL_SENSCO2_Z1_N102   = 'Sensors_N102_N102_Co2'

# nr records of 5 mins?
# 1 day    288
# 5 days   1440
# 1 week   2016

# train set
# start date = 22oct2019
# end data   = 10nov2019
# nr. days = 20
# nr. records = 288 * nr. days
# nr. records = 5760
# UPPER TRAIN LIMIT = nr. records - nr. inputs
# UPPER TRAIN LIMIT = 5760 - 10
LOWERLIM_RECS5MINS_TRAIN = 0
UPPERLIM_RECS5MINS_TRAIN = 5750

# test set
# start date = 11nov2019
# end date   = 15nov2019
# nr. days = 5
# nr. records = 288 * nr. days
# nr. records = 288 * 5
# nr. records = 1440
# UPPER TEST LIMIT = LOWER TEST LIMIT + nr. records
LOWERLIM_RECS5MINS_TEST = 5750
UPPERLIM_RECS5MINS_TEST = 7190


class DataProcessing:

    def __init__(self):
        FILE_NAME = 'Data_GECAD_Nbuilding 14Oct-15Nov2019.csv'
        self.dadosIniciais = pd.read_csv(FILE_NAME)
        self.dadosEstruturados = []

    def construct_input_data_output_data(self):
        dadosIniciais = self.dadosIniciais
        NR_ROWS = len(dadosIniciais.index)
        NR_COLS = 13
        dadosEstruturados = np.zeros((NR_ROWS-10, NR_COLS))
        e = 0

        for i in range(10, NR_ROWS):
            dadosEstruturados[e, 0]  = dadosIniciais.loc[i-1,  COL_CONS_Z1]
            dadosEstruturados[e, 1]  = dadosIniciais.loc[i-2,  COL_CONS_Z1]
            dadosEstruturados[e, 2]  = dadosIniciais.loc[i-3,  COL_CONS_Z1]
            dadosEstruturados[e, 3]  = dadosIniciais.loc[i-4,  COL_CONS_Z1]
            dadosEstruturados[e, 4]  = dadosIniciais.loc[i-5,  COL_CONS_Z1]
            dadosEstruturados[e, 5]  = dadosIniciais.loc[i-6,  COL_CONS_Z1]
            dadosEstruturados[e, 6]  = dadosIniciais.loc[i-7,  COL_CONS_Z1]
            dadosEstruturados[e, 7]  = dadosIniciais.loc[i-8,  COL_CONS_Z1]
            dadosEstruturados[e, 8]  = dadosIniciais.loc[i-9,  COL_CONS_Z1]
            dadosEstruturados[e, 9]  = dadosIniciais.loc[i-10, COL_CONS_Z1]
            dadosEstruturados[e, 10] = dadosIniciais.loc[i - 1, COL_DALI_LIGHT_Z1]
            dadosEstruturados[e, 11] = dadosIniciais.loc[i - 1, COL_SENSCO2_Z1_N102]
            dadosEstruturados[e, 12] = dadosIniciais.loc[i, COL_CONS_Z1]
            e = e + 1

        self.dadosEstruturados = np.array(dadosEstruturados, np.float64)

    def save_in_excel(self):
        dadosEstruturados = self.dadosEstruturados

        df_matrix = pd.DataFrame(dadosEstruturados[LOWERLIM_RECS5MINS_TRAIN:UPPERLIM_RECS5MINS_TRAIN])
        df_matrix.to_csv(os.getcwd() + '//train_data.csv')
        df_matrix = pd.DataFrame(dadosEstruturados[LOWERLIM_RECS5MINS_TEST:UPPERLIM_RECS5MINS_TEST])
        df_matrix.to_csv(os.getcwd() + '//test_data.csv')
