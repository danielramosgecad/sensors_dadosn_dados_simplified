import numpy as np
from tensorflow import keras
import calculations as calculations
from sklearn.ensemble import RandomForestRegressor
import numpy as np
from algorithm import Algorithm

class RF(Algorithm):
    def __init__(self, train_input, train_output, test_input):
        #self.train_input1 = np.array(train_input1)
        #self.train_output1 = np.array(train_output1)
        #self.test_input1 = np.array(test_input1)

        #train_input2 = []
        #train_output2 = []
        #test_input2 = []

        #indexs_dayweek = np.array([[1,0,0,0,0,0,0],[0,1,0,0,0,0,0],[0,0,1,0,0,0,0],[0,0,0,1,0,0,0],[0,0,0,0,1,0,0],
        #                  [0,0,0,0,0,1,0],[0,0,0,0,0,0,1]])

        #for i in range(len(train_input)):
        #    day_week = int(train_input[i][0])
        #    array = []
        #    #for j in range(len(indexs_dayweek[0])):
        #    #    array.append(indexs_dayweek[day_week][j])
        #    for j in range(1,len(train_input[0])):
        #        array.append(train_input[i][j])
        #    train_input2.append(array)

        #for i in range(len(test_input)):
        #    day_week = int(test_input[i][0])
        #    array = []
        #    #for j in range(len(indexs_dayweek[0])):
        #    #    array.append(indexs_dayweek[day_week][j])
        #    for j in range(1,len(test_input[0])):
        #        array.append(test_input[i][j])
        #    test_input2.append(array)

        #self.train_input = np.array(train_input2, np.float64)
        #self.train_output = np.array(train_output, np.float64)
        #self.test_input = np.array(test_input2, np.float64)

        self.train_input = np.array(train_input, np.float64)
        self.train_output = np.array(train_output, np.float64)
        self.test_input = np.array(test_input, np.float64)

    def predict(self):
        rf = RandomForestRegressor(n_estimators = 1000, random_state = 42) # modelo random forest

        rf.fit(self.train_input, self.train_output); # treina os dados

        test_predictions = rf.predict(self.test_input) # cálculo das previsões

        # Prints
        for i in range(len(test_predictions)):
            print('\nPrevisão (RF) = ', test_predictions[i])

        return test_predictions
