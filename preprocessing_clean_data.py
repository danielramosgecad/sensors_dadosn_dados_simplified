import numpy as np
import pandas as pd
import xlsxwriter
import datetime

class DataProcessing:

    INITIAL_VALUE_SPLIT_MINUTES_NOW = 0
    INITIAL_VALUE_SPLIT_MINUTES_NEXT = 0
    days_of_week = ['monday', 'tuesday', 'wednesday', 'thrusday', 'friday', 'saturday', 'sunday']


    def __init__(self, full_path, file_name, sheet_name, datetime_parameter, target_parameter):
        self.full_path = full_path
        self.file_name = file_name
        self.sheet_name = sheet_name
        self.datetime_parameter = datetime_parameter
        self.target_parameter = target_parameter

        DataProcessing.INITIAL_VALUE_SPLIT_MINUTES_NOW = 0
        DataProcessing.INITIAL_VALUE_SPLIT_MINUTES_NEXT = 5


    def handle_missing_values(self):
        self.df = pd.read_excel(self.full_path, self.sheet_name)

        matrix_db = []
        index_excel_row_1 = 1
        index_excel_row_2 = 1

        b_save_data = 1
        b_missing_5minutes = 0
        consumption = 0
        hours_now = 0
        hours_next = 0
        day_now = 22
        day_next = 22
        month_now = 5
        month_next = 5
        year_now = 2017
        year_next = 2017
        split_minutes_now = DataProcessing.INITIAL_VALUE_SPLIT_MINUTES_NOW
        split_minutes_next = DataProcessing.INITIAL_VALUE_SPLIT_MINUTES_NEXT
        consumptions = []
        for i in self.df.index:
            date_i = self.df[self.datetime_parameter][i]
            consumption_i = self.df[self.target_parameter][i]

            print(date_i)
            # print(consumption_i)
            temp_date_1 = date_i.split(" ")
            temp_date_11 = temp_date_1[0].split("-")
            temp_date_12 = temp_date_1[1].split(":")

            year = temp_date_11[0]
            month = temp_date_11[1]
            day = temp_date_11[2]
            hours = temp_date_12[0]
            minutes = temp_date_12[1]

            hours = int(hours)
            minutes = int(minutes)

            # Checks in which 5 minutes you are right now
            if (minutes >= 55):
                minutes = 55
            else:
                if (minutes >= 50):
                    minutes = 50
                else:
                    if (minutes >= 45):
                        minutes = 45
                    else:
                        if (minutes >= 40):
                            minutes = 40
                        else:
                            if (minutes >= 35):
                                minutes = 35
                            else:
                                if (minutes >= 30):
                                    minutes = 30
                                else:
                                    if (minutes >= 25):
                                        minutes = 25
                                    else:
                                        if (minutes >= 20):
                                            minutes = 20
                                        else:
                                            if (minutes >= 15):
                                                minutes = 15
                                            else:
                                                if (minutes >= 10):
                                                    minutes = 10
                                                else:
                                                    if (minutes >= 5):
                                                        minutes = 5
                                                    else:
                                                        minutes = 0

            #ind_of_week = datetime.datetime(int(year), int(month), int(day), hours, minutes).weekday()
            #
            # keep including data from monday
            #if (ind_of_week == 0):
            #    b_save_data = 1
            # data frp, sundays must be excluded
            #if (ind_of_week == 6):
            #    b_save_data = 0

            int_year = int(year)
            int_month = int(month)
            int_day = int(day)

#            if(int_month == 1):
#                if(int_day == 3):
#                    print("debug from this point on")

            # handle missing 5 minutes records
            b_sum_consumptions = 0
            if (b_save_data == 1):
                if (minutes == split_minutes_now):
                    if(hours == hours_now):
                        if(int_day == day_now):
                            if(int_month == month_now):
                                if(int_year == year_now):
                                    consumption = consumption + consumption_i
                                    b_sum_consumptions = 1
                if(b_sum_consumptions == 0):
                    b_missing_5minutes = 0
                    if(minutes != split_minutes_next):
                        b_missing_5minutes = 1
                    if(hours != hours_next):
                        b_missing_5minutes = 1
                    if (int_day != day_next):
                        b_missing_5minutes = 1
                    if (int_month != month_next):
                        b_missing_5minutes = 1
                    if (int_year != year_next):
                        b_missing_5minutes = 1

                    consumptions.append(consumption)
                    if (hours_now == 23):
                        if (split_minutes_now == 50):
                            save_day = day
                            save_month = month
                            save_year = year
                    if (hours_now == 23):
                        if (split_minutes_now == 55):
                            day = save_day
                            month = save_month
                            year = save_year
                    matrix_db.append([year_now, month_now, day_now, hours_now, split_minutes_now, consumption])
                    consumption = 0
                    index_excel_row_1 = index_excel_row_1 + 1
                    index_excel_row_2 = index_excel_row_2 + 1

                    while (b_missing_5minutes):
                        previous_consumption = consumptions[len(consumptions) - 1]
                        consumptions.append(previous_consumption)
                        split_minutes_now = split_minutes_now + 5
                        split_minutes_next = split_minutes_next + 5
                        if (split_minutes_now == 60):
                            split_minutes_now = 0
                            hours_now = hours_now + 1
                            if (hours_now == 24):
                                hours_now = 0
                                if (day_now == 31):
                                    if (month_now == 1 or month_now == 3 or month_now == 5 or month_now == 7 or month_now == 8 or month_now == 10):
                                        month_now = month_now + 1
                                    if (month_now == 12):
                                        year_now = year_now + 1
                                        month_now = 1
                                    day_now = 1
                                elif (day_now == 30 and (
                                        month_now == 4 or month_now == 6 or month_now == 9 or month_now == 11)):
                                    month_now = month_now + 1
                                    day_now = 1
                                elif (month_now == 2 and (day_now == 28 or day_now == 29)):
                                    # ano bixesto
                                    if (day_now == 28 or (
                                            day_now == 29 and (
                                            (year_now % 4 == 0 and year_now % 100 != 0) or (year_now % 400 == 0)))):
                                        month_now = month_now + 1
                                        day_now = 1
                                else:
                                    day_now = day_now + 1
                        if (split_minutes_next == 60):
                            split_minutes_next = 0
                            hours_next = hours_next + 1
                            if (hours_next == 24):
                                hours_next = 0
                                if (day_next == 31):
                                    if (month_next == 1 or month_next == 3 or month_next == 5 or month_next == 7 or month_next == 8 or month_next == 10):
                                        month_next = month_next + 1
                                    if (month_next == 12):
                                        year_next = year_next + 1
                                        month_next = 1
                                    day_next = 1
                                elif (day_next == 30 and (month_next == 4 or month_next == 6 or month_next == 9 or month_next == 11)):
                                    month_next = month_next + 1
                                    day_next = 1
                                elif (month_next == 2 and (day_next == 28 or day_next == 29)):
                                    # ano bixesto
                                    if (day_next == 28 or (day_next == 29 and ((year_next % 4 == 0 and year_next % 100 != 0) or (
                                            year_next % 400 == 0)))):
                                        month_next = month_next + 1
                                        day_next = 1
                                else:
                                    day_next = day_next + 1

                        if (hours_now == 23):
                            if (split_minutes_now == 50):
                                save_day = day
                                save_month = month
                                save_year = year
                        if (hours_now == 23):
                            if (split_minutes_now == 55):
                                day = save_day
                                month = save_month
                                year = save_year
                        matrix_db.append([year_now, month_now, day_now, hours_now, split_minutes_now, previous_consumption])
                        index_excel_row_1 = index_excel_row_1 + 1
                        index_excel_row_2 = index_excel_row_2 + 1

                        b_missing_5minutes = 0
                        if (minutes != split_minutes_next):
                            b_missing_5minutes = 1
                        if (hours != hours_next):
                            b_missing_5minutes = 1
                        if(int_day != day_next):
                            b_missing_5minutes = 1
                        if(int_month != month_next):
                            b_missing_5minutes = 1
                        if(int_year != year_next):
                            b_missing_5minutes = 1

                    split_minutes_now = split_minutes_now + 5
                    split_minutes_next = split_minutes_next + 5
                    if (split_minutes_now == 60):
                        split_minutes_now = 0
                        hours_now = hours_now + 1
                        if (hours_now == 24):
                            hours_now = 0
                            if(day_now == 31):
                                if (month_now == 1 or month_now == 3 or month_now == 5 or month_now == 7 or month_now == 8 or month_now == 10):
                                    month_now = month_now + 1
                                if(month_now == 12):
                                    year_now = year_now + 1
                                    month_now = 1
                                day_now = 1
                            elif (day_now == 30 and (month_now == 4 or month_now == 6 or month_now == 9 or month_now == 11)):
                                month_now = month_now + 1
                                day_now = 1
                            elif (month_now == 2 and (day_now == 28 or day_now == 29)):
                                # ano bixesto
                                if (day_now == 28 or (
                                        day_now == 29 and ((year_now % 4 == 0 and year_now % 100 != 0) or (year_now % 400 == 0)))):
                                    month_now = month_now + 1
                                    day_now = 1
                            else:
                                day_now = day_now + 1
                    if (split_minutes_next == 60):
                        split_minutes_next = 0
                        hours_next = hours_next + 1
                        if (hours_next == 24):
                            hours_next = 0
                            if (day_next == 31):
                                if (month_next == 1 or month_next == 3 or month_next == 5 or month_next == 7 or month_next == 8 or month_next == 10):
                                    month_next = month_next + 1
                                if (month_next == 12):
                                    year_next = year_next + 1
                                    month_next = 1
                                day_next = 1
                            elif (day_next == 30 and (
                                    month_next == 4 or month_next == 6 or month_next == 9 or month_next == 11)):
                                month_next = month_next + 1
                                day_next = 1
                            elif (month_next == 2 and (day_next == 28 or day_next == 29)):
                                # ano bixesto
                                if (day_next == 28 or (day_next == 29 and (
                                        (year_next % 4 == 0 and year_next % 100 != 0) or (year_next % 400 == 0)))):
                                    month_next = month_next + 1
                                    day_next = 1
                            else:
                                day_next = day_next + 1
                    consumption = consumption_i

        # last iteration
        consumptions.append(consumption)
        day = save_day
        month = save_month
        year = save_year
        matrix_db.append([year, month, day, hours_now, split_minutes_now, consumption])

        self.matrix_db = np.array(matrix_db)


    def handle_outliers(self):
        vec_aux = self.matrix_db[:, 5].astype(np.float)
        mean = np.mean(vec_aux, axis=0)
        sd = np.std(vec_aux, axis=0)
        index = 0
        for consumption_i in vec_aux:
            if (index > 0):
                if (index < len(vec_aux) - 1):
                    if (consumption_i <= mean - 2 * sd):
                        aux = vec_aux[index - 1] + vec_aux[index + 1]
                        self.matrix_db[index, 5] = aux / 2
                    if (consumption_i >= mean + 2 * sd):
                        aux = vec_aux[index - 1] + vec_aux[index + 1]
                        self.matrix_db[index, 5] = aux / 2
            index = index + 1

        matrix_db = self.matrix_db
        self.matrix_db = np.array(matrix_db)


    def save_cleaned_data(self):
        file_name_without_extension = self.file_name[0:len(self.file_name)-5]
        file_save_name = 'Cleaned_Data_' + file_name_without_extension + '_' + self.target_parameter + '.xlsx'
        self.wb = xlsxwriter.Workbook(file_save_name)
        self.sheet = self.wb.add_worksheet()

        self.sheet.write(0, 0, 'A')
        self.sheet.write(0, 1, 'M')
        self.sheet.write(0, 2, 'D')
        self.sheet.write(0, 3, 'H')
        self.sheet.write(0, 4, 'm')
        self.sheet.write(0, 5, self.sheet_name + '_' + self.target_parameter)

        for i in range(len(self.matrix_db)):
            self.sheet.write(i + 1, 0, self.matrix_db[i][0])
            self.sheet.write(i + 1, 1, self.matrix_db[i][1])
            self.sheet.write(i + 1, 2, self.matrix_db[i][2])
            self.sheet.write(i + 1, 3, self.matrix_db[i][3])
            self.sheet.write(i + 1, 4, self.matrix_db[i][4])
            self.sheet.write(i + 1, 5, str(self.matrix_db[i][5]))

        self.wb.close()
