import numpy as np
import pandas as pd
import csv
import xlsxwriter

class DataProcessing:
    def __init__(self, father_path, file_name):
        self.father_path = father_path
        self.file_name = file_name

    def read_data(self):
        titles = []
        matrix_db = []
        count = 0
        path = self.father_path + '//' + self.file_name
        with open(path) as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            for row in csvReader:
                if(count == 0):
                    for i in range(len(row)):
                        titles.append(row[i])
                        print(row[i])
                    arr_titles = np.array(titles)
                    self.matrix_titles = np.asmatrix(arr_titles)
                else:
                    matrix_db.append(row)
                    print(row)
                count = count+1
        self.matrix_db = np.array(matrix_db)
        print(self.matrix_titles)
        print(self.matrix_db)

    def handle_outliers_v1(self):
        self.file_end_name = 'v1'
        total_count = 1
        for c in range(total_count):
            vec_aux = self.matrix_db[:, 5].astype(np.float)
            index = 0
            for consumption_i in vec_aux:
                vec_aux2 = np.delete(vec_aux, index)
                vec_aux3 = vec_aux2[index - 36:index + 36]
                mean = np.mean(vec_aux3, axis=0)
                sd = np.std(vec_aux3, axis=0)
                print(consumption_i)
                if (index > 0):
                    if (index < len(vec_aux) - 1):
                        if (consumption_i <= mean - 2 * sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                        if (consumption_i >= mean + 2 * sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                index = index + 1

        matrix_db = self.matrix_db
        self.matrix_db = np.array(matrix_db)

    def handle_outliers_v2(self):
        self.file_end_name = 'v2'
        total_count = 1
        for c in range(total_count):
            vec_aux = self.matrix_db[:, 5].astype(np.float)
            index = 0
            for consumption_i in vec_aux:
                vec_aux2 = np.delete(vec_aux, index)
                vec_aux3 = vec_aux2[index - 36:index + 36]
                mean = np.mean(vec_aux3, axis=0)
                sd = np.std(vec_aux3, axis=0)
                #print(consumption_i)
                if (index > 0):
                    if (index < len(vec_aux) - 1):
                        if (consumption_i <= 6 * mean - 2 * sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                        if (consumption_i >= 6 * mean + 2 * sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                index = index + 1

        matrix_db = self.matrix_db
        self.matrix_db = np.array(matrix_db)

    def handle_outliers_v3(self):
        self.file_end_name = 'v3'
        total_count = 1
        for c in range(total_count):
            vec_aux = self.matrix_db[:, 5].astype(np.float)
            index = 0
            for consumption_i in vec_aux:
                vec_aux2 = np.delete(vec_aux, index)
                vec_aux3 = vec_aux2[index - 36:index + 36]
                mean = np.mean(vec_aux3, axis=0)
                sd = np.std(vec_aux3, axis=0)
                print(consumption_i)
                if (index > 0):
                    if (index < len(vec_aux) - 1):
                        if (consumption_i <= mean - sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                        if (consumption_i >= mean + sd):
                            aux = vec_aux[index - 1] + vec_aux[index + 1]
                            self.matrix_db[index, 5] = aux / 2
                index = index + 1

        matrix_db = self.matrix_db
        self.matrix_db = np.array(matrix_db)

    def save_cleaned_data(self):
        res = np.append(self.matrix_titles, self.matrix_db, axis=0)
        file_name = 'Cleaned_data_Consumption_' + self.file_end_name + '.csv'
        np.savetxt(file_name, res, delimiter=",", fmt='%s')
