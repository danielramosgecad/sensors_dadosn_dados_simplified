import tensorflow as tf
import numpy as np
from tensorflow import keras
from algorithm import Algorithm
import calculations as calculations

EPOCHS = 500

class PrintDot(keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs):
        if epoch % 100 == 0: print('')
        print('.', end='')

def build_model(train_input, n_outputs):
    model = keras.Sequential([
        keras.layers.Dense(64, activation=tf.nn.relu,
                           input_shape=(train_input.shape[1],)),
        keras.layers.Dense(64, activation=tf.nn.relu),
        keras.layers.Dense(n_outputs)
    ])

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    optimizer = tf.contrib.estimator.clip_gradients_by_norm(optimizer, 5.0)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae'])

    return model

class ANN(Algorithm):
    def __init__(self, train_input, train_output, test_input, n_outputs):
        self.train_input = np.array(train_input)
        self.train_output = np.array(train_output)
        self.test_input = np.array(test_input)
        self.n_outputs = n_outputs

    def predict(self):
        # Regressão para os dados de entrada
        #mean = calculations.calculate_mean(self.train_input)
        #std = calculations.calculate_std(self.train_input)
        #self.train_input = (self.train_input - mean) / std
        #self.test_input = (self.test_input - mean) / std

        # Construção do modelo
        model = build_model(self.train_input, self.n_outputs)
        model.summary()

        # Stop no treino da rede quando não se obtém mais melhorias
        early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)

        # Treina os dados e devolve histórico
        self.history = model.fit(self.train_input, self.train_output, epochs=EPOCHS,
                                 validation_split=0.2, verbose=0,
                                 callbacks=[early_stop, PrintDot()])
        test_predictions = model.predict(self.test_input) # cálculo das previsões

        # Prints
        for i in range(len(test_predictions)):
            print('\nPrevisão (ANN) = ', test_predictions[i])

        return test_predictions
