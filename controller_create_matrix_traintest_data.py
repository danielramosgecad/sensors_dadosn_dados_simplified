from preprocessing_creatematrix_tar11_15nov2019_hist20days_x10cons_sens_z1 import DataProcessing


class CreateMatrixTrainTestDataController:
    def __init__(self):
        self.process_matrixdata = DataProcessing()

    # Extrai dados de treino e de teste pertencentes à base de dados
    def create_matrix_traintest_data(self):
        self.process_matrixdata.construct_input_data_output_data()
        self.process_matrixdata.save_in_excel()
