import numpy as np

def calculate_average(array):
    n = len(array) # quantidade de dados
    # Calcula e imprime a média
    media = 0
    for i in range(n):
        media = media + array[i]
    media = media / n
    return media

def calculate_mean(train_input):
    return train_input.mean(axis=0)

def calculate_std(train_input):
    return train_input.std(axis=0)

def calculate_error(test_predictions, test_output):
    return test_predictions - test_output

def calculate_MAPE(test_predictions, test_output):
    return np.mean(np.abs((test_output - test_predictions) / test_output)) * 100

def calculate_MSE(test_predictions, test_output):
    return np.square(np.subtract(test_output, test_predictions)).mean()
