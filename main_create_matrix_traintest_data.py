import tensorflow as tf

from controller_create_matrix_traintest_data import CreateMatrixTrainTestDataController

def main(unused_argv):
    # Cria a matriz de treino e teste
    controller = CreateMatrixTrainTestDataController()
    controller.create_matrix_traintest_data()

if __name__ == "__main__":
    tf.app.run()
