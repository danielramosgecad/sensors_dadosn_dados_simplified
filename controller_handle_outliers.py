from preprocessing_handle_outliers import DataProcessing
import os

class HandleOutliersController:
    def __init__(self):
        self.process_handle_data_cz1_v1 = DataProcessing(
            os.getcwd(),
            'Data_GECAD_Nbuilding 22oct-15nov2019.csv')

    def handle_data(self):
        self.process_handle_data_cz1_v1.read_data()
        self.process_handle_data_cz1_v1.handle_outliers_v1()
        self.process_handle_data_cz1_v1.save_cleaned_data()
