import xlsxwriter

class DataProcessing:
    def __init__(self, test_output, allpredictions):
        self.test_output = test_output
        self.allpredictions = allpredictions
        #self.predictions_ann = allpredictions[0]
        #self.predictions_svm = allpredictions[1]
        #self.predictions_rf = allpredictions[2]

    def export_file(self):
        print("get here")
        test_output = self.test_output
        allpredictions = self.allpredictions

        self.wb = xlsxwriter.Workbook('results.xlsx')
        self.sheet = self.wb.add_worksheet()

        for j in range(len(test_output)):
            print(test_output[j])
            self.sheet.write(j+1, 0, str(test_output[j]))

        for i in range(len(allpredictions)):
            for j in range(len(test_output)):
                self.sheet.write(j+1, i+1, allpredictions[i][j])

        self.wb.close()
