import tensorflow as tf

from controller_clean_data import CleanDataController

def main(unused_argv):
    controller = CleanDataController()
    controller.handle_data()

if __name__ == "__main__":
    tf.app.run()
