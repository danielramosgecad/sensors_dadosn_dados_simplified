import os
import pandas as pd
import numpy as np

class DataProcessing:

    def __init__(self):
        self.train_input = []
        self.train_output = []
        self.test_input = []
        self.test_output = []
        self.n_outputs = 1

    def train_data(self):
        df = pd.read_csv(os.getcwd() + '//train_data.csv')
        train_data = df.to_numpy()

        self.train_data = np.transpose(train_data)
        train_input = self.train_data[0:101]
        self.train_input = np.transpose(train_input)
        train_output = self.train_data[101]
        self.train_output = np.transpose(train_output)


    def test_data(self):
        df = pd.read_csv(os.getcwd() + '//test_data.csv')
        test_data = df.to_numpy()

        self.test_data = np.transpose(test_data)
        test_input = self.test_data[0:101]
        self.test_input = np.transpose(test_input)
        test_output = self.test_data[101]
        self.test_output = np.transpose(test_output)
