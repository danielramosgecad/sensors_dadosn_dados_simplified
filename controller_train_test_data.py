import numpy as np
import pandas as pd
import sys

from preprocessing_traintestdata_cons_z1_12inp1out import DataProcessing


class TrainTestDataController:
    def __init__(self):
        self.process_predictdata = DataProcessing()

    # Extrai dados de treino e de teste pertencentes à base de dados
    def train_and_test_data(self):
        self.process_predictdata.train_data()
        self.process_predictdata.test_data()

    def fetch_train_input(self):
        return self.process_predictdata.train_input

    def fetch_train_output(self):
        return self.process_predictdata.train_output

    def fetch_test_input(self):
        return self.process_predictdata.test_input

    def fetch_test_output(self):
        return self.process_predictdata.test_output

    def fetch_n_outputs(self):
        return self.process_predictdata.n_outputs

