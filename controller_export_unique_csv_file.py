import pandas as pd
import numpy as np


class ExportUniqueCsvFileController:
    def __init__(self):
        # file name, col ref, sheet name
        self.info = [
                ["Cleaned_Data_Consumption_Zone_1_total", "Consumption_Zone_1_total"],
                ["Cleaned_Data_Consumption_Zone_2_total", "Consumption_Zone_2_total"],
                ["Cleaned_Data_Consumption_Zone_3_total", "Consumption_Zone_3_total"],
                ["Cleaned_Data_Consumption_Zone_Corridor_total", "Consumption_Zone_Corridor_total"],
                ["Cleaned_Data_PV_total", "PV_total"],
                ["Cleaned_Data_Door_Status_Zone_1_N101_Door_NO", "Sensors_N102_N101_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_1_N102_Door_NO", "Sensors_N102_N102_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_1_N103_Door_NO", "Sensors_N102_N103_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_2_N104_Door_NO", "Sensors_Zone_2_N104_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_2_N105_Door_NO", "Sensors_Zone_2_N105_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_2_N106_Door_NO", "Sensors_Zone_2_N106_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_3_N107_Door_NO", "Sensors_zone3_N107_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_3_N108_Door_NO", "Sensors_zone3_N108_Door_NO"],
                ["Cleaned_Data_Door_Status_Zone_3_N109_Door_NO", "Sensors_zone3_N109_Door_NO"],
                ["Cleaned_Data_Movement_Sensors_N102_Movement_Sensor_N102_1", "Sensors_N102_Movement_Sensor_N102_1"],
                ["Cleaned_Data_Movement_Sensors_N102_Movement_Sensor_N102_2", "Sensors_N102_Movement_Sensor_N102_2"],
                ["Cleaned_Data_Movement_Sensors_N102_Movement_Sensor_N102_3", "Sensors_N102_Movement_Sensor_N102_3"],
                ["Cleaned_Data_Movement_Sensors_N102_Movement_Sensor_N102_4", "Sensors_N102_Movement_Sensor_N102_4"],
                ["Cleaned_Data_Movement_Sensors_N105_Movement_Sensor_N105_1", "Sensors_N105_Movement_Sensor_N105_1"],
                ["Cleaned_Data_Movement_Sensors_N105_Movement_Sensor_N105_2", "Sensors_N105_Movement_Sensor_N105_2"],
                ["Cleaned_Data_Movement_Sensors_N107_N107_Movement_Contact", "Sensors_N107_N107_Movement_Contact"],
                ["Cleaned_Data_DALI Lights Values_Light_N101_1", "DALI Lights Values_Light_N101_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N101_2", "DALI Lights Values_Light_N101_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N102_1", "DALI Lights Values_Light_N102_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N102_2", "DALI Lights Values_Light_N102_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N103_1", "DALI Lights Values_Light_N103_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N103_2", "DALI Lights Values_Light_N103_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N105_1", "DALI Lights Values_Light_N105_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N105_2", "DALI Lights Values_Light_N105_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N106_1", "DALI Lights Values_Light_N106_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N106_2", "DALI Lights Values_Light_N106_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N107", "DALI Lights Values_Light_N107"],
                ["Cleaned_Data_DALI Lights Values_Light_N108_1", "DALI Lights Values_Light_N108_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N108_2", "DALI Lights Values_Light_N108_2"],
                ["Cleaned_Data_DALI Lights Values_Light_N109_1", "DALI Lights Values_Light_N109_1"],
                ["Cleaned_Data_DALI Lights Values_Light_N109_2", "DALI Lights Values_Light_N109_2"],
                ["Cleaned_Data_DALI Lights Values_Corridor_1", "DALI Lights Values_Corridor_1"],
                ["Cleaned_Data_DALI Lights Values_Corridor_2", "DALI Lights Values_Corridor_2"],
                ["Cleaned_Data_DALI Lights Values_Corridor_3", "DALI Lights Values_Corridor_3"],
                ["Cleaned_Data_DALI Lights Values_Corridor_4", "DALI Lights Values_Corridor_4"],
                ["Cleaned_Data_Sensors_Lights_Outside_AND_Corridor_Light_Sensor_Outside", "Sensors_lights_Light_Sensor_Outside"],
                ["Cleaned_Data_Sensors_Lights_Outside_AND_Corridor_Corridor_Light_x10", "Sensors_lights_Corridor_Light_x10"],
                ["Cleaned_Data_Sensors_N102_N102_Co2", "Sensors_N102_N102_Co2"],
                ["Cleaned_Data_Sensors_N102_N102_VOC_Air_Quality_x10", "Sensors_N102_N102_VOC_Air_Quality_x10"],
                ["Cleaned_Data_Sensors_N102_N102_Temperature_x10", "Sensors_N102_N102_Temperature_x10"],
                ["Cleaned_Data_Sensors_N102_N102_Humidity_x10", "Sensors_N102_N102_Humidity_x10"],
                ["Cleaned_Data_Sensors_N102_N102_Light_x10", "Sensors_N102_N102_Light_x10"],
                ["Cleaned_Data_Sensors_N104_N104_Co2", "Sensors_N104_N104_Co2"],
                ["Cleaned_Data_Sensors_N104_N104_VOC_Air_Quality_x10", "Sensors_N104_N104_VOC_Air_Quality_x10"],
                ["Cleaned_Data_Sensors_N104_N104_Temperature_x10", "Sensors_N104_N104_Temperature_x10"],
                ["Cleaned_Data_Sensors_N104_N104_Humidity_x10", "Sensors_N104_N104_Humidity_x10"],
                ["Cleaned_Data_Sensors_N105_N105_Co2", "Sensors_N105_N105_Co2"],
                ["Cleaned_Data_Sensors_N105_N105_VOC_Air_Quality_x10", "Sensors_N105_N105_VOC_Air_Quality_x10"],
                ["Cleaned_Data_Sensors_N105_N105_Temperature_x10", "Sensors_N105_N105_Temperature_x10"],
                ["Cleaned_Data_Sensors_N105_N105_Humidity_x10", "Sensors_N105_N105_Humidity_x10"],
                ["Cleaned_Data_Sensors_N105_N105_Light_x10", "Sensors_N105_N105_Light_x10"],
                ["Cleaned_Data_Sensors_N107_N107_Humidity_x10", "Sensors_zone3_N107_Humidity_x10"],
                ["Cleaned_Data_Sensors_N107_N107_Temperature_x10", "Sensors_zone3_N107_Temperature_x10"]
        ]


    def export_cleaned_data(self):
        info = self.info
        #folder = "(Cleaned data)Building N\\Low storage files"
        folder = "(Cleaned data)Building N"

        nr_files = len(info)
        mat_db = np.zeros((265824, 63))
        arr_namecols = []
        for f in range(nr_files):
            file_path = folder + '\\' + info[f][0] + '.xlsx'
            df = pd.read_excel(file_path)
            print("Read file ", info[f][1])
            if (f == 0):
                arr_namecols.append('A')
                arr_namecols.append('M')
                arr_namecols.append('D')
                arr_namecols.append('H')
                arr_namecols.append('m')
                for i in df.index:
                    time_periods = df.iloc[i, [0, 1, 2, 3, 4]]
                    mat_db[i][0] = time_periods[0]
                    mat_db[i][1] = time_periods[1]
                    mat_db[i][2] = time_periods[2]
                    mat_db[i][3] = time_periods[3]
                    mat_db[i][4] = time_periods[4]
            arr_namecols.append(info[f][1])
            for j in df.index:
                target = df.iloc[j, [5]]
                mat_db[j][5+f] = str(target[0])
            print("Next file on its way")
        arr_namecols = np.array(arr_namecols)
        mat_namecols = np.asmatrix(arr_namecols)
        print(mat_namecols)

        #res = np.stack(arr_namecols, mat_db)
        #res = mat_db.insert(0, arr_namecols)
        res = np.append(mat_namecols, mat_db, axis = 0)
        np.savetxt("Cleaned_data_GECAD_N_building.csv", res, delimiter=",", fmt='%s')
        # alternative for dataframe file save
        #df_keep = pd.DataFrame(columns=matrix_refs[:,1])
        #ADD CODE HERE
        #df_keep.to_csv("Cleaned_data_GECAD_N_building.csv")


    def export_cleaned_data_the_hard_way(self):
        matrix_refs = []
        df = pd.read_excel("Consult_cleaned_data_structure.xlsx", "Sheet1")
        directory = df['directory'][0]
        for i in df.index:
            file_name = df['file'][i]
            column_ref = df['column_ref'][i]
            file_path = directory + '\\' + file_name + '.xlsx'
            matrix_refs.append([file_path, column_ref])
        matrix_refs = np.array(matrix_refs)

        matrix_db = []
        df2 = pd.read_excel(matrix_refs[0][0], "Sheet1")
        vec_year = []
        vec_month = []
        vec_day = []
        vec_hour = []
        vec_min = []
        for ind in df2.index:
            year_index = df2["A"][ind]
            month_index = df2["M"][ind]
            day_index = df2["D"][ind]
            hour_index = df2["H"][ind]
            min_index = df2["m"][ind]
            vec_year.append([year_index])
            vec_month.append([month_index])
            vec_day.append([day_index])
            vec_hour.append([hour_index])
            vec_min.append([min_index])
        vec_year = np.array(vec_year)
        vec_month = np.array(vec_month)
        vec_day = np.array(vec_day)
        vec_hour = np.array(vec_hour)
        vec_min = np.array(vec_min)
        matrix_db = np.hstack((vec_year, vec_month))
        matrix_db = np.hstack((matrix_db, vec_month))
        matrix_db = np.hstack((matrix_db, vec_day))
        matrix_db = np.hstack((matrix_db, vec_hour))
        matrix_db = np.hstack((matrix_db, vec_min))
        for i in df.index:
            df2 = pd.read_excel(matrix_refs[i][0], "Sheet1")
            set_targets = []
            for j in df2.index:
                target = df2[matrix_refs[i][1]][j]
                print(target)
                set_targets.append([target])
            set_targets = np.array(set_targets)
            matrix_db = np.hstack((matrix_db, set_targets))
        matrix_db = np.array(matrix_db)
        print(matrix_db[0])

        np.savetxt("Cleaned_data_GECAD_N_building.csv", matrix_db)
