import numpy as np
from tensorflow import keras
#from sklearn import svm
from sklearn.neighbors import KNeighborsRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.metrics import accuracy_score
from algorithm import Algorithm
import calculations as calculations
import pdb

class SVM(Algorithm):
    def __init__(self, train_input, train_output, test_input):
        self.train_input = np.array(train_input)
        self.train_output = np.array(train_output)
        self.test_input = np.array(test_input)

    def predict(self):
        # treina os dados e devolve histórico
        knn = KNeighborsRegressor()
        #regr = MultiOutputRegressor(knn)
        regr = knn
        regr.fit(self.train_input, self.train_output)

        test_predictions = regr.predict(self.test_input) # cálculo das previsões
        for i in range(len(test_predictions)):
            print('\nPrevisão (SVM) = ', test_predictions[i])

        return test_predictions
