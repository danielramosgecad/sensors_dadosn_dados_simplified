import tensorflow as tf
from preprocessing_clean_data import DataProcessing

from controller_clean_data import CleanDataController

def main(unused_argv):
    process_handle_data_s1 = DataProcessing(
        'Building N\\Sensors\\Sensors_N102.xlsx',
        'Sensors_N102.xlsx', 'Sensors_N102',
        'datetime', 'N102_Temperature_x10')
    process_handle_data_s1.handle_missing_values()
    process_handle_data_s1.save_cleaned_data()


if __name__ == "__main__":
    tf.app.run()
