import tensorflow as tf
from controller_export_unique_csv_file import ExportUniqueCsvFileController

def main(unused_argv):
    controller = ExportUniqueCsvFileController()
    controller.export_cleaned_data()

if __name__ == "__main__":
    tf.app.run()
