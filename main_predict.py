import tensorflow as tf

from controller_train_test_data import TrainTestDataController
from controller_predict import PredictController
from preprocessing_export_predictions import DataProcessing

def main(unused_argv):
    ####### Load de dados #######
    # Dá início ao load de dados
    controller = TrainTestDataController()
    #controller.start_notifying_user_ongoing_task()
    controller.train_and_test_data()
    #controller.stop_notifying_user_ongoing_task()

    # Passa os dados de treino e teste para serem usados como instância da classe
    train_input = controller.fetch_train_input()
    train_output = controller.fetch_train_output()
    test_input = controller.fetch_test_input()
    test_output = controller.fetch_test_output()
    n_outputs = controller.fetch_n_outputs()

    # Dados carregados para o programa principal
    print("\nData loaded. Ready to predict.")

    ####### Previsões #######
    # Dá início à realização de previsões
    pc = PredictController(train_input, train_output,
                           test_input, n_outputs)

    pc.add_algorithm("ANN")
    pc.add_algorithm("SVM")
    pc.add_algorithm("RF")
    # pc.start_notifying_user_ongoing_task()
    pc.predict_ANN()
    pc.predict_SVM()
    # pc.predict_RF()
    # pc.stop_notifying_user_ongoing_task()

    # Reconstrução dos dados associados a previsões
    allalgorithms = pc.fetch_allalgorithms()
    allpredictions = pc.fetch_allpredictions()

    print(allpredictions[0])

    # Previsões realizadas
    print("\nPredicted.")

    process = DataProcessing(test_output, allpredictions)
    process.export_file()

if __name__ == "__main__":
    tf.app.run()
