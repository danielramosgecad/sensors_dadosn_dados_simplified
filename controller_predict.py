import numpy as np

from artificialneuralnetworks import ANN
from supportvectormachine import SVM
from randomforest import RF

class PredictController:
    def __init__(self, train_input, train_output, test_input, n_outputs):
        self.train_input = train_input
        self.train_output = train_output
        self.test_input = test_input
        self.n_outputs = n_outputs

        self.allpredictions = []
        self.allalgorithms = []

    def add_algorithm(self, txt_algorithm):
        self.allalgorithms.append(txt_algorithm)

    def predict_ANN(self):
        self.algorithm1 = ANN(self.train_input, self.train_output, self.test_input, self.n_outputs)
        test_predictions = self.algorithm1.predict()
        self.allpredictions.append(test_predictions)

    def predict_SVM(self):
        algorithm2 = SVM(self.train_input, self.train_output, self.test_input)
        test_predictions = algorithm2.predict()
        self.allpredictions.append(test_predictions)

    def predict_RF(self):
        algorithm3 = RF(self.train_input, self.train_output, self.test_input)
        test_predictions = algorithm3.predict()
        self.allpredictions.append(test_predictions)

    def fetch_allalgorithms(self):
        return np.array(self.allalgorithms)

    def fetch_allpredictions(self):
        return self.allpredictions
        #return np.array(self.allpredictions, np.float64)
