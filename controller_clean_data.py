from preprocessing_clean_data import DataProcessing

class CleanDataController:
    def __init__(self):
        self.process_handle_data_cz1 = DataProcessing(
            'Building N\\Consumption and PV\\Consumption_Zone_1.xlsx',
            'Consumption_Zone_1.xlsx', 'Consumption_Zone_1',
            'datetime', 'total')
        self.process_handle_data_cz2 = DataProcessing(
            'Building N\\Consumption and PV\\Consumption_Zone_2.xlsx',
            'Consumption_Zone_2.xlsx', 'Consumption_Zone_2',
            'datetime', 'total')
        self.process_handle_data_cz3 = DataProcessing(
            'Building N\\Consumption and PV\\Consumption_Zone_3.xlsx',
            'Consumption_Zone_3.xlsx', 'Consumption_Zone_3',
            'datetime', 'total')
        self.process_handle_data_czc = DataProcessing(
            'Building N\\Consumption and PV\\Consumption_Zone_Corridor.xlsx',
            'Consumption_Zone_Corridor.xlsx', 'Consumption_Zone_Corridor',
            'datetime', 'total')
        self.process_handle_data_PV = DataProcessing(
            'Building N\\Consumption and PV\\PV.xlsx',
            'PV.xlsx', 'PV',
            'datetime', 'total')

        self.process_handle_data_dsz1 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_1.xlsx',
            'Door_Status_Zone_1.xlsx', 'Sensors_N102',
            'datetime', 'N101_Door_NO')
        self.process_handle_data_dsz2 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_1.xlsx',
            'Door_Status_Zone_1.xlsx', 'Sensors_N102',
            'datetime', 'N102_Door_NO')
        self.process_handle_data_dsz3 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_1.xlsx',
            'Door_Status_Zone_1.xlsx', 'Sensors_N102',
            'datetime', 'N103_Door_NO')
        self.process_handle_data_dsz4 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_2.xlsx',
            'Door_Status_Zone_2.xlsx', 'Sensors_Zone_2',
            'datetime', 'N104_Door_NO')
        self.process_handle_data_dsz5 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_2.xlsx',
            'Door_Status_Zone_2.xlsx', 'Sensors_Zone_2',
            'datetime', 'N105_Door_NO')
        self.process_handle_data_dsz6 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_2.xlsx',
            'Door_Status_Zone_2.xlsx', 'Sensors_Zone_2',
            'datetime', 'N106_Door_NO')
        self.process_handle_data_dsz7 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_3.xlsx',
            'Door_Status_Zone_3.xlsx', 'Sensors_zone3',
            'datetime', 'N107_Door_NO')
        self.process_handle_data_dsz8 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_3.xlsx',
            'Door_Status_Zone_3.xlsx', 'Sensors_zone3',
            'datetime', 'N108_Door_NO')
        self.process_handle_data_dsz9 = DataProcessing(
            'Building N\\Door Status\\Door_Status_Zone_3.xlsx',
            'Door_Status_Zone_3.xlsx', 'Sensors_zone3',
            'datetime', 'N109_Door_NO')

        self.process_handle_data_ms1 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N102.xlsx',
            'Movement_Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'Movement_Sensor_N102_1')
        self.process_handle_data_ms2 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N102.xlsx',
            'Movement_Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'Movement_Sensor_N102_2')
        self.process_handle_data_ms3 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N102.xlsx',
            'Movement_Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'Movement_Sensor_N102_3')
        self.process_handle_data_ms4 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N102.xlsx',
            'Movement_Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'Movement_Sensor_N102_4')
        self.process_handle_data_ms5 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N105.xlsx',
            'Movement_Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'Movement_Sensor_N105_1')
        self.process_handle_data_ms6 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N105.xlsx',
            'Movement_Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'Movement_Sensor_N105_2')
        self.process_handle_data_ms7 = DataProcessing(
            'Building N\\Movement Sensors\\Movement_Sensors_N107.xlsx',
            'Movement_Sensors_N107.xlsx', 'Sensors_N107',
            'datetime', 'N107_Movement_Contact')

        self.process_handle_data_s1 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N101_1')
        self.process_handle_data_s2 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N101_2')
        self.process_handle_data_s3 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N102_1')
        self.process_handle_data_s4 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N102_2')
        self.process_handle_data_s5 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N103_1')
        self.process_handle_data_s6 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N103_2')
        self.process_handle_data_s7 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N105_1')
        self.process_handle_data_s8 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N105_2')
        self.process_handle_data_s9 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N106_1')
        self.process_handle_data_s10 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N106_2')
        self.process_handle_data_s11 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N107')
        self.process_handle_data_s12 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N108_1')
        self.process_handle_data_s13 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N108_2')
        self.process_handle_data_s14 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N109_1')
        self.process_handle_data_s15 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Light_N109_2')
        self.process_handle_data_s16 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Corridor_1')
        self.process_handle_data_s17 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Corridor_2')
        self.process_handle_data_s18 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Corridor_3')
        self.process_handle_data_s19 = DataProcessing(
            'Building N\\Sensors\\DALI Lights Values.xlsx',
            'DALI Lights Values.xlsx', 'DALI Lights Values',
            'datetime', 'Corridor_4')
        self.process_handle_data_s20 = DataProcessing(
            'Building N\\Sensors\\Sensors_Lights_Outside_AND_Corridor.xlsx',
            'Sensors_Lights_Outside_AND_Corridor.xlsx', 'Sensors_lights',
            'datetime', 'Light_Sensor_Outside')
        self.process_handle_data_s21 = DataProcessing(
            'Building N\\Sensors\\Sensors_Lights_Outside_AND_Corridor.xlsx',
            'Sensors_Lights_Outside_AND_Corridor.xlsx', 'Sensors_lights',
            'datetime', 'Corridor_Light_x10')
        self.process_handle_data_s22 = DataProcessing(
            'Building N\\Sensors\\Sensors_N102.xlsx',
            'Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'N102_Co2')
        self.process_handle_data_s23 = DataProcessing(
            'Building N\\Sensors\\Sensors_N102.xlsx',
            'Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'N102_VOC_Air_Quality_x10')
        self.process_handle_data_s24 = DataProcessing(
            'Building N\\Sensors\\Sensors_N102.xlsx',
            'Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'N102_Temperature_x10')
        self.process_handle_data_s25 = DataProcessing(
            'Building N\\Sensors\\Sensors_N102.xlsx',
            'Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'N102_Humidity_x10')
        self.process_handle_data_s26 = DataProcessing(
            'Building N\\Sensors\\Sensors_N102.xlsx',
            'Sensors_N102.xlsx', 'Sensors_N102',
            'datetime', 'N102_Light_x10')
        self.process_handle_data_s27 = DataProcessing(
            'Building N\\Sensors\\Sensors_N104.xlsx',
            'Sensors_N104.xlsx', 'Sensors_N104',
            'datetime', 'N104_Co2')
        self.process_handle_data_s28 = DataProcessing(
            'Building N\\Sensors\\Sensors_N104.xlsx',
            'Sensors_N104.xlsx', 'Sensors_N104',
            'datetime', 'N104_VOC_Air_Quality_x10')
        self.process_handle_data_s29 = DataProcessing(
            'Building N\\Sensors\\Sensors_N104.xlsx',
            'Sensors_N104.xlsx', 'Sensors_N104',
            'datetime', 'N104_Temperature_x10')
        self.process_handle_data_s30 = DataProcessing(
            'Building N\\Sensors\\Sensors_N104.xlsx',
            'Sensors_N104.xlsx', 'Sensors_N104',
            'datetime', 'N104_Humidity_x10')
        self.process_handle_data_s31 = DataProcessing(
            'Building N\\Sensors\\Sensors_N105.xlsx',
            'Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'N105_Co2')
        self.process_handle_data_s32 = DataProcessing(
            'Building N\\Sensors\\Sensors_N105.xlsx',
            'Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'N105_VOC_Air_Quality_x10')
        self.process_handle_data_s33 = DataProcessing(
            'Building N\\Sensors\\Sensors_N105.xlsx',
            'Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'N105_Temperature_x10')
        self.process_handle_data_s34 = DataProcessing(
            'Building N\\Sensors\\Sensors_N105.xlsx',
            'Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'N105_Humidity_x10')
        self.process_handle_data_s35 = DataProcessing(
            'Building N\\Sensors\\Sensors_N105.xlsx',
            'Sensors_N105.xlsx', 'Sensors_N105',
            'datetime', 'N105_Light_x10')
        self.process_handle_data_s36 = DataProcessing(
            'Building N\\Sensors\\Sensors_N107.xlsx',
            'Sensors_N107.xlsx', 'Sensors_zone3',
            'datetime', 'N107_Humidity_x10')
        self.process_handle_data_s37 = DataProcessing(
            'Building N\\Sensors\\Sensors_N107.xlsx',
            'Sensors_N107.xlsx', 'Sensors_zone3',
            'datetime', 'N107_Temperature_x10')


    def handle_data(self):
        self.process_handle_data_cz1.handle_missing_values()
        self.process_handle_data_cz1.save_cleaned_data()
        self.process_handle_data_cz2.handle_missing_values()
        self.process_handle_data_cz2.save_cleaned_data()
        self.process_handle_data_cz3.handle_missing_values()
        self.process_handle_data_cz3.save_cleaned_data()
        self.process_handle_data_czc.handle_missing_values()
        self.process_handle_data_czc.save_cleaned_data()
        self.process_handle_data_PV.handle_missing_values()
        self.process_handle_data_PV.save_cleaned_data()

        self.process_handle_data_dsz1.handle_missing_values()
        self.process_handle_data_dsz1.save_cleaned_data()
        self.process_handle_data_dsz2.handle_missing_values()
        self.process_handle_data_dsz2.save_cleaned_data()
        self.process_handle_data_dsz3.handle_missing_values()
        self.process_handle_data_dsz3.save_cleaned_data()
        self.process_handle_data_dsz4.handle_missing_values()
        self.process_handle_data_dsz4.save_cleaned_data()
        self.process_handle_data_dsz5.handle_missing_values()
        self.process_handle_data_dsz5.save_cleaned_data()
        self.process_handle_data_dsz6.handle_missing_values()
        self.process_handle_data_dsz6.save_cleaned_data()
        self.process_handle_data_dsz7.handle_missing_values()
        self.process_handle_data_dsz7.save_cleaned_data()
        self.process_handle_data_dsz8.handle_missing_values()
        self.process_handle_data_dsz8.save_cleaned_data()
        self.process_handle_data_dsz9.handle_missing_values()
        self.process_handle_data_dsz9.save_cleaned_data()

        self.process_handle_data_ms1.handle_missing_values()
        self.process_handle_data_ms1.save_cleaned_data()
        self.process_handle_data_ms2.handle_missing_values()
        self.process_handle_data_ms2.save_cleaned_data()
        self.process_handle_data_ms3.handle_missing_values()
        self.process_handle_data_ms3.save_cleaned_data()
        self.process_handle_data_ms4.handle_missing_values()
        self.process_handle_data_ms4.save_cleaned_data()
        self.process_handle_data_ms5.handle_missing_values()
        self.process_handle_data_ms5.save_cleaned_data()
        self.process_handle_data_ms6.handle_missing_values()
        self.process_handle_data_ms6.save_cleaned_data()
        self.process_handle_data_ms7.handle_missing_values()
        self.process_handle_data_ms7.save_cleaned_data()

        self.process_handle_data_s1.handle_missing_values()
        self.process_handle_data_s1.save_cleaned_data()
        self.process_handle_data_s2.handle_missing_values()
        self.process_handle_data_s2.save_cleaned_data()
        self.process_handle_data_s3.handle_missing_values()
        self.process_handle_data_s3.save_cleaned_data()
        self.process_handle_data_s4.handle_missing_values()
        self.process_handle_data_s4.save_cleaned_data()
        self.process_handle_data_s5.handle_missing_values()
        self.process_handle_data_s5.save_cleaned_data()
        self.process_handle_data_s6.handle_missing_values()
        self.process_handle_data_s6.save_cleaned_data()
        self.process_handle_data_s7.handle_missing_values()
        self.process_handle_data_s7.save_cleaned_data()
        self.process_handle_data_s8.handle_missing_values()
        self.process_handle_data_s8.save_cleaned_data()
        self.process_handle_data_s9.handle_missing_values()
        self.process_handle_data_s9.save_cleaned_data()
        self.process_handle_data_s10.handle_missing_values()
        self.process_handle_data_s10.save_cleaned_data()
        self.process_handle_data_s11.handle_missing_values()
        self.process_handle_data_s11.save_cleaned_data()
        self.process_handle_data_s12.handle_missing_values()
        self.process_handle_data_s12.save_cleaned_data()
        self.process_handle_data_s13.handle_missing_values()
        self.process_handle_data_s13.save_cleaned_data()
        self.process_handle_data_s14.handle_missing_values()
        self.process_handle_data_s14.save_cleaned_data()
        self.process_handle_data_s15.handle_missing_values()
        self.process_handle_data_s15.save_cleaned_data()
        self.process_handle_data_s16.handle_missing_values()
        self.process_handle_data_s16.save_cleaned_data()
        self.process_handle_data_s17.handle_missing_values()
        self.process_handle_data_s17.save_cleaned_data()
        self.process_handle_data_s18.handle_missing_values()
        self.process_handle_data_s18.save_cleaned_data()
        self.process_handle_data_s19.handle_missing_values()
        self.process_handle_data_s19.save_cleaned_data()
        self.process_handle_data_s20.handle_missing_values()
        self.process_handle_data_s20.save_cleaned_data()
        self.process_handle_data_s21.handle_missing_values()
        self.process_handle_data_s21.save_cleaned_data()
        self.process_handle_data_s22.handle_missing_values()
        self.process_handle_data_s22.save_cleaned_data()
        self.process_handle_data_s23.handle_missing_values()
        self.process_handle_data_s23.save_cleaned_data()
        self.process_handle_data_s24.handle_missing_values()
        self.process_handle_data_s24.save_cleaned_data()
        self.process_handle_data_s25.handle_missing_values()
        self.process_handle_data_s25.save_cleaned_data()
        self.process_handle_data_s26.handle_missing_values()
        self.process_handle_data_s26.save_cleaned_data()
        self.process_handle_data_s27.handle_missing_values()
        self.process_handle_data_s27.save_cleaned_data()
        self.process_handle_data_s28.handle_missing_values()
        self.process_handle_data_s28.save_cleaned_data()
        self.process_handle_data_s29.handle_missing_values()
        self.process_handle_data_s29.save_cleaned_data()
        self.process_handle_data_s30.handle_missing_values()
        self.process_handle_data_s30.save_cleaned_data()
        self.process_handle_data_s31.handle_missing_values()
        self.process_handle_data_s31.save_cleaned_data()
        self.process_handle_data_s32.handle_missing_values()
        self.process_handle_data_s32.save_cleaned_data()
        self.process_handle_data_s33.handle_missing_values()
        self.process_handle_data_s33.save_cleaned_data()
        self.process_handle_data_s34.handle_missing_values()
        self.process_handle_data_s34.save_cleaned_data()
        self.process_handle_data_s35.handle_missing_values()
        self.process_handle_data_s35.save_cleaned_data()
        self.process_handle_data_s36.handle_missing_values()
        self.process_handle_data_s36.save_cleaned_data()
        self.process_handle_data_s37.handle_missing_values()
        self.process_handle_data_s37.save_cleaned_data()
